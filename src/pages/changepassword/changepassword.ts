import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import { MemeProvider } from '../../providers/meme/meme';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'page-changepassword',
   templateUrl: 'changepassword.html',
 })
 export class ChangepasswordPage {

   loading: any;
   ChangePasswordData = { oldpassword:'', password:'', cpassword:'' };
   data: any;

   passwordType: string = 'password';
   newpasswordType: string = 'password';
   confirmpasswordType: string = 'password';

   passwordIcon: string = 'eye-off';
   newpasswordIcon: string = 'eye-off';
   confirmpasswordIcon: string = 'eye-off';

   notificationsCount:any;

   constructor(private loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams,
   public alertCtrl: AlertController,public memeservice: MemeProvider,public storage: Storage) {

   }

   hideShowPassword() {
     this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
     this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  newhideShowPassword() {
    this.newpasswordType = this.newpasswordType === 'text' ? 'password' : 'text';
    this.newpasswordIcon = this.newpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  confirmhideShowPassword() {
    this.confirmpasswordType = this.confirmpasswordType === 'text' ? 'password' : 'text';
    this.confirmpasswordIcon = this.confirmpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

   // openMyAccountPopover(myEvent) {
   //   let popover = this.popoverCtrl.create(MyaccountpopoverPage);
   //   popover.present({
   //     ev: myEvent
   //   });
   // }

   ionViewDidLoad() {
     console.log('ionViewDidLoad ChangePasswordPage');
     this.notificationsCount = localStorage.getItem("BooksForYounotificationsCount");
   }

    ActionChangePassword() {

      if(navigator.onLine !== true) {
        console.log("not online")
        this.presentAlert('No Internet Connection!',"Please put data connection on and try again");
      }
    else if (this.ChangePasswordData.oldpassword === "" || this.ChangePasswordData.password === "" || this.ChangePasswordData.cpassword === "") {
      this.presentAlert('',"All fields must be filled");
        }
        else if (this.ChangePasswordData.password !== this.ChangePasswordData.cpassword) {
        this.presentAlert('',"Password fields do not match");
        }
        else {

          let mmloading = this.loadingCtrl.create({ content: 'Please wait...' });
          mmloading.present();

          this.memeservice.changePassword(this.ChangePasswordData).then((result) => {
            mmloading.dismissAll();
            this.data = result;
            localStorage.setItem('token', this.data.access_token);
            console.log("brian-----"+JSON.stringify(this.data));
            let status_code = this.data.status_code;
            let message = this.data.message;
            if(status_code == 1) {
              this.ChangePasswordData.oldpassword =null;
              this.ChangePasswordData.password =null;
              this.ChangePasswordData.cpassword = null;
              this.storage.set('farmUphasLoggedIn', 'false');
               this.navCtrl.setRoot('LoginPage');
              this.presentAlert('',message);
            }
            else {
              this.presentAlert('',message);
            }

          }, (err) => {
            mmloading.dismissAll();
            this.presentAlert('',"Sorry, we are experiencing a technical challenge");
          });

  }
  }

  presentAlert(alerttitle,alertcontent) {
   const alert = this.alertCtrl.create({
     title: alerttitle,
     subTitle: alertcontent,
     buttons: ['Ok']
   });
   alert.present();
 }

 // presentAlertNetwork(alerttitle,alertcontent) {
 // const alert = this.alertCtrl.create({
 //  title: alerttitle,
 //  subTitle: alertcontent,
 //  buttons: ['Ok']
 // });
 // alert.present();
 // alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(SettingsPage), 500); });
 // }


 }
