import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, LoadingController } from 'ionic-angular';
import { MemeProvider } from '../../providers/meme/meme';
import { UserPage } from '../user/user';

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-people',
  templateUrl: 'people.html',
})
export class PeoplePage {
  list:Array<any>;

  loading:any;
  data: any;
  errorMessage: string;
  page = 1;
  order = 2;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  nodataavailable:boolean = false;

  bannerText:any;
  bannerImage:any;
  bannerUrl:any;
  showbanner:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    private loadingCtrl:LoadingController,public memeservice: MemeProvider) {

  }

  doRefresh(refresher){
    this.page = 1;
    this.loadList();

     if(refresher)
        refresher.complete();
  }

  loadList() {
      if(navigator.onLine !== true) {
        this.presentAlert('No Internet Connection!',"Please put data connection on and try again");
      }
      else {
        let loading = this.loadingCtrl.create({});
    loading.present();
    this.memeservice.loadProfiles(this.page)
       .then(
         res => {
           loading.dismissAll();
           this.data = res;

           this.list = this.data.result.data;
           this.perPage = this.data.result.per_page;
           this.totalData = this.data.result.total;
           this.totalPage = this.data.result.total_pages;

           //Banner
           if(this.data.result.showbanner == true) {
             this.bannerText = this.data.result.bannerText;
             this.bannerImage = this.data.result.bannerImage;
             this.bannerUrl = this.data.result.bannerUrl;
             this.showbanner = true;
           }

           if(this.data.result.total == 0) {
             this.nodataavailable = true;
           }
           else {
             this.nodataavailable = false;
           }
         },
         error =>  this.errorMessage = <any>error);
  }
}

doInfinite(infiniteScroll) {
this.page = this.page+1;
setTimeout(() => {
  this.memeservice.loadProfiles(this.page)
     .then(
       res => {
         this.data = res;
         this.perPage = this.data.result.per_page;
         this.totalData = this.data.result.total;
         this.totalPage = this.data.result.total_pages;
         for(let i=0; i<this.data.result.data.length; i++) {
           this.list.push(this.data.result.data[i]);
         }
       },
       error =>  this.errorMessage = <any>error);

  console.log('Async operation has ended');
  infiniteScroll.complete();
}, 1000);
}

presentAlert(alerttitle,alertcontent) {
const alert = this.alertCtrl.create({
title: alerttitle,
subTitle: alertcontent,
buttons: ['Ok']
});
alert.present();
//alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
}

open(userId,fullName,nickName,profilePic,count,memberSince) {
  this.navCtrl.push(UserPage,{
    userId:userId,
    fullName:fullName,
    nickName:nickName,
    profilePic:profilePic,
    count:count,
    memberSince:memberSince
  });
}

ionViewDidLoad() {
  console.log('ionViewDidLoad PeoplePage');
}

ionViewDidEnter() {
  this.loadList();
}

openBanner() {
  if(this.bannerUrl != null) {
    window.open(this.bannerUrl, '_system');
  }
}

}
