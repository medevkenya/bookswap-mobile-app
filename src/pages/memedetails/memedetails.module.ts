import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MemedetailsPage } from './memedetails';

@NgModule({
  declarations: [
    MemedetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MemedetailsPage),
  ],
})
export class MemedetailsPageModule {}
