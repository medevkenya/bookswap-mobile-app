import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, LoadingController, AlertController, ToastController, App } from 'ionic-angular';
import { MemeProvider } from '../../providers/meme/meme';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import {File,FileEntry} from "@ionic-native/file";
//import { AndroidPermissions } from '@ionic-native/android-permissions';

import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the BookdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-memedetails',
  templateUrl: 'memedetails.html',
})
export class MemedetailsPage {

  public like_btn = {
    color: 'black',
    icon_name: 'heart-outline'
  };

  public tap: number = 0;

  public listitem;
  loading:any;
  data:any;

  item:any;
  payload:any;

  download_btn_color:any;
  share_btn_color:any;

  downloadingText:any;
  showdownloadingText:boolean = false;

  bannerText:any;
  bannerImage:any;
  bannerUrl:any;
  showbanner:boolean = false;

  showButtons:boolean = false;

  constructor(
    private toastCtrl: ToastController,
    //private androidPermissions: AndroidPermissions,
    private file: File,
    private transfer: FileTransfer,
    private socialSharing: SocialSharing,
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private loadingCtrl:LoadingController,
    //private photoViewer: PhotoViewer,
    public memeservice: MemeProvider,
    private app:App,
  ) {
    this.download_btn_color = 'light';
    this.share_btn_color = 'light';
    this.item = this.navParams.get('listitem');

    if(this.item == undefined || this.item == "" || !this.item || this.item ==null) {
      this.payload = this.navParams.get('payload');
      this.item = JSON.parse(this.payload);
      this.showButtons = true;
    }
  }

  openBanner() {
    if(this.bannerUrl != null) {
      window.open(this.bannerUrl, '_system');
    }
  }

  openTabs() {
    this.app.getRootNav().setRoot(TabsPage);
  }

  viewedMeme(){
    if(navigator.onLine !== true) {
      this.presentAlert('No Internet Connection!',"Please put data connection on and try again");
    }
    else {

  let loading = this.loadingCtrl.create({});
  loading.present();
   this.memeservice.viewMeme(this.item.id)
   .then(result => {
     this.data = result;
     this.item = this.data.result;

     //Banner
     if(this.item.showbanner == true) {
       this.bannerText = this.item.bannerText;
       this.bannerImage = this.item.bannerImage;
       this.bannerUrl = this.item.bannerUrl;
       this.showbanner = true;
     }

     loading.dismissAll();
     console.log("viewMeme------"+JSON.stringify(this.item));
   });
 }
   }

   presentAlert(alerttitle,alertcontent) {
     const alert = this.alertCtrl.create({
     title: alerttitle,
     subTitle: alertcontent,
     buttons: ['Ok']
     });
     alert.present();
     //alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
   }

   likeButton() {

       this.like_btn.icon_name = 'heart';
       this.like_btn.color = 'secondary';

       this.item.likes = this.item.likes + 1;

     let loading = this.loadingCtrl.create({});
     loading.present();
     this.memeservice.likeItem(this.item.id)
    .then(result => {
      this.data = result;
      this.presentToast(this.data.message);
      this.item.liked = 1;
      loading.dismissAll();
      //console.log("likeButton------"+JSON.stringify(this.item));
    });

   }

   likedAlready() {
     this.presentToast('Liked!');
   }

   // downloadButton() {
   //
   //   this.download_btn_color = 'secondary';
   //
   //  //  this.transfer.download(this.item.file, 'file.pdf').then((entry) => {
   //  //   console.log('download complete: ' + entry.toURL());
   //  // }, (err) => {
   //  //   // handle error
   //  //   console.log('Error',err);
   //  // });
   //
   // }

   async downloadFile() {

     let loading = this.loadingCtrl.create({ duration: 5000, content: 'Downloading...' });
     loading.present();

     this.download_btn_color = 'secondary';

     const fileTransfer: FileTransferObject = this.transfer.create();

     //**Progress Bar**
       fileTransfer.onProgress((e)=>
       {

         if (e) {
                // console.log("download progress success =====")
             } else {
                 //console.log("download progress error =====")
             }

         let prg=(e.lengthComputable) ?  Math.round(e.loaded / e.total * 100) : -1;
         //console.log("file download progress:"+prg);
         this.showdownloadingText = true;
         this.downloadingText = 'Download progress...'+prg+'%';
         //loader.setContent(prg);
       });

       const url = this.item.file;
      fileTransfer.download(url, this.file.externalRootDirectory + '/Download/memesasa-' + Date.now() +'.'+ this.item.fileExtension).then((entry) => {
        //console.log('download complete: ' + entry.toURL());
        this.showdownloadingText = false;
        loading.dismissAll();
        this.presentToast('Completed. Check downloads folder');
      }, (error) => {
        // handle error
        //console.log('Error',error);
        this.showdownloadingText = false;
        loading.dismissAll();
        this.presentToast('Request failed');
      });

      //  fileTransfer.download(this.item.file, this.item.file).then((entry) => {
      //   console.log('download complete: ' + entry.toURL());
      // }, (err) => {
      //   // handle error
      //   console.log('Error',err);
      // });

  //await fileTransfer.download(this.item.file, this.file.externalRootDirectory + '/Download/' + "soap-bubble-1959327_960_720.jpg");
}

presentToast(text) {
  let toast = this.toastCtrl.create({
    message: text,
    duration: 5000,
    position: 'top'
    // cssClass:'toast-bg',
    // showCloseButton:true,
    // closeButtonText: "Open"
  });

  toast.onDidDismiss((data, role) => {
    console.log('Dismissed toast');
    if (role == "close") {
      this.openDownloadedFile();
    }
  });

  toast.present();
}

openDownloadedFile() {

}

// getPermission() {
//   this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
//     .then(status => { console.log("getPermission---1");
//       if (status.checkPermission) {
//         this.downloadFile(); console.log("getPermission---2");
//       }
//       else {
//         this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
//           .then(status => { console.log("getPermission---3");
//             if(status.checkPermission) { console.log("getPermission---4");
//               this.downloadFile();
//             }
//           });
//       }
//     });
// }

   tapPhotoLike(times) { // If we click double times, it will trigger like the post
     this.tap++;
     if(this.tap % 2 === 0) {
       this.likeButton();
     }
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookdetailsPage');
  }

  ionViewDidEnter() {
    this.viewedMeme();
  }

  // viewPhoto(url,title) {
  //   this.photoViewer.show(url, title, {share: false});
  // }

  shareMeme() {
    this.share_btn_color = 'secondary';
  let options = {
                      message: 'Memesasa', // not supported on some apps (Facebook, Instagram)
                      subject: 'Memesasa', // fi. for email
                      files: [this.item.file], // an array of filenames either locally or remotely
                      url: 'https://play.google.com/store/apps/details?id=memesasa.mobile.app&hl=en_US',
                      chooserTitle: 'Share this meme with' // Android only, you can override the default share sheet title
                  }
                  this.socialSharing.shareWithOptions(options).then((result) => {
                      //console.log("Share completed? ", result.completed); // On Android apps mostly return false even while it's true
                      //console.log("Shared to app: ", result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
                  }, (err) => {
                      //console.log("Sharing failed with message: ", err);
                  });

}

}
