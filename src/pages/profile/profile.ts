import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController ,AlertController, LoadingController, App } from 'ionic-angular';
import { MemeProvider } from '../../providers/meme/meme';
import { EditProfile } from '../edit-profile/edit-profile';
import { Options } from '../options/options';
import { TaggedProfile } from '../tagged-profile/tagged-profile';
import { SavedProfile } from '../saved-profile/saved-profile';
//import { SigninPage } from '../signin/signin';
import { PostPage } from '../post/post';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class Profile {

  public profile_segment:string;

  fullName:any;
  nickName:any;
  profilePic:any;
  count:any;
  memberSince:any;
  notifyMe:any;
  profileDetails:any;

  list:Array<any>;

  loading:any;
  data: any;
  errorMessage: string;
  page = 1;
  order = 2;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  nodataavailable:boolean = false;

  showButtons:boolean = false;

  public flag;

  constructor(
    public alertCtrl: AlertController,
    private loadingCtrl:LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public memeservice: MemeProvider,
    public storage: Storage,
    private app:App
  ) {
    // //check if is logged in
    //   this.storage.get('MemesUphasLoggedIn')
    //   .then((MemesUphasLoggedIn) => {
    //     if (MemesUphasLoggedIn == "true") {
    //
    //     }
    //     else {
    //       this.app.getRootNav().setRoot(SigninPage, {page:'Profile'});
    //       // let modal = this.modalCtrl.create(SigninPage, {page:'Profile'});
    //       // // modal.onDidDismiss(data => {
    //       // //   this.loadProfile();
    //       // // });
    //       // modal.present();
    //     }
    //   });
    //   //end check if logged in

      if(this.navParams.get('flag')==1) {
        this.showButtons = true;
      }

      this.list = [
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
        {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'}
      ];

  }

  openPost() {
    //this.app.getRootNav().setRoot(PostPage);
    this.navCtrl.push(PostPage);
  }

  openTabs() {
    this.app.getRootNav().setRoot(TabsPage);
  }

  doRefresh(refresher){
    this.page = 1;
    this.loadProfile();
     if(refresher)
        refresher.complete();
  }

  loadProfile(){
    let loading = this.loadingCtrl.create({});
    loading.present();
 this.memeservice.getProfile(this.page)
 .then(result => {
   loading.dismissAll();
   this.data = result;
   //console.log('loadProfile---'+JSON.stringify(this.data.result.list.data));
   this.profileDetails = this.data.result.details;
   this.fullName = this.profileDetails.fullName;
   this.nickName = this.profileDetails.nickName;
   this.profilePic = this.profileDetails.profilePic;
   this.count = this.profileDetails.count;
   this.memberSince = this.profileDetails.memberSince;
   this.notifyMe = this.profileDetails.notifyMe;

   if(this.data.result.list.total == 0) {
     this.nodataavailable = true;
   }
   else {
     this.list = [];
     this.nodataavailable = false;
   }

   this.list = this.data.result.list.data;
   this.perPage = this.data.result.list.per_page;
   this.totalData = this.data.result.list.total;
   this.totalPage = this.data.result.list.total_pages;

 });
 }

 doInfinite(infiniteScroll) {
 this.page = this.page+1;
 setTimeout(() => {
   this.memeservice.getProfile(this.page)
      .then(
        res => {
          this.data = res;
          this.perPage = this.data.result.list.per_page;
          this.totalData = this.data.result.list.total;
          this.totalPage = this.data.result.list.total_pages;

          for(let i=0; i<this.data.result.list.data.length; i++) {
            //console.log('getProfile---'+JSON.stringify(this.data.result.list.data[i]));
            this.list.push(this.data.result.list.data[i]);
          }
        },
        error =>  this.errorMessage = <any>error);

   //console.log('Async operation has ended');
   infiniteScroll.complete();
 }, 1000);
 }

 presentAlert(alerttitle,alertcontent) {
 const alert = this.alertCtrl.create({
 title: alerttitle,
 subTitle: alertcontent,
 buttons: ['Ok']
 });
 alert.present();
 alert.onDidDismiss(() => {  setTimeout(() => this.loadProfile(), 500); });
 }

  ionViewDidLoad() {

  }

  ionViewDidEnter() {
    //check if is logged in
      this.storage.get('MemesUphasLoggedIn')
      .then((MemesUphasLoggedIn) => {
        if (MemesUphasLoggedIn == "true") {
          this.loadProfile();
        }
      });
      //end check if logged in
  }

  remove(id) {
    const confirm = this.alertCtrl.create({
      title: 'You want remove this meme?',
      //message: 'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Agree clicked');
            this.removeItem(id);
          }
        }
      ]
    });
    confirm.present();
  }

  removeItem(id){
    let loading = this.loadingCtrl.create({ duration: 5000, content: 'Loading...' });
    loading.present();
     this.memeservice.removeItem(id)
     .then(result => {
       loading.dismissAll();
       this.data = result;
       this.presentAlert('',this.data.message);
     });
 }

  // Define segment for everytime when profile page is active
  ionViewWillEnter() {
    this.profile_segment = 'grid';
  }

  goEditProfile() {
    // Open it as a modal page
    let modal = this.modalCtrl.create(EditProfile, {profileDetails:this.profileDetails});
    modal.onDidDismiss(data => {
      this.loadProfile();
    });
    modal.present();
  }

  goOptions() {
    this.navCtrl.push(Options, {notifyMe:this.notifyMe});
  }

  goTaggedProfile() {
    this.navCtrl.push(TaggedProfile);
  }

  goSavedProfile() {
    this.navCtrl.push(SavedProfile);
  }

  // Triggers when user pressed a post
  pressPhoto(user_id: number, username: string, profile_img: string, post_img: string) {
    //this.presentModal(user_id, username, profile_img, post_img);
  }

  // Set post modal
  // presentModal(user_id: number, username: string, profile_img: string, post_img: string) {
  //   let modal = this.modalCtrl.create(ModalPost,
  //   { // Send data to modal
  //     user_id: user_id,
  //     username: username,
  //     profile_img: profile_img,
  //     post_img: post_img
  //   }, // This data comes from API!
  //   { showBackdrop: true, enableBackdropDismiss: true });
  //   modal.present();
  // }

}
