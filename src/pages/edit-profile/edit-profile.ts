import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { MemeProvider } from '../../providers/meme/meme';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ActionSheetController } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfile {

  ProfileData = { fullName:'', nickName:'', dob:'', gender:'' };

  profilePic:any;

  data:any;
  loading:any;

  public profileDetails;

  public base64Image: string;
  public fileImage: string;
  public responseData: any;
  userData = { appId: "", imageB64: "" };

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public memeservice: MemeProvider,
    public actionSheetCtrl: ActionSheetController,
    public camera: Camera,
    public toastCtrl:ToastController,
    private sanitizer: DomSanitizer
    ) {
      this.profileDetails = this.navParams.get('profileDetails');
      this.ProfileData.fullName = this.profileDetails.fullName;
      this.ProfileData.nickName = this.profileDetails.nickName;
      this.ProfileData.dob = this.profileDetails.dob;
      this.ProfileData.gender = this.profileDetails.gender;
      this.profilePic = this.profileDetails.profilePic;
  }

  updateProfile() {

  //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if(navigator.onLine !== true) {
    console.log("not online")
    this.presentAlert('Internet Connection',"Please put data connection on and try again");
  }
  else if (this.ProfileData.dob === "" || this.ProfileData.fullName === "" || this.ProfileData.gender === "") {
    this.presentAlert('',"All fields must be filled");
  }
  else if (this.ProfileData.fullName.length < 3) {
    this.presentAlert('',"Please enter a valid full name");
  }
  // else if (this.ProfileData.nickName.length < 3) {
  //   this.presentAlert('',"Enter a valid nick name");
  //   }
  // else if(!re.test(this.ProfileData.dob)) {
  //   // Invalid dob
  //   this.presentAlert('',"Please enter a valid dob address");
  // }
  else {

  let loading = this.loadingCtrl.create({});
  loading.present();

  this.memeservice.editProfile(this.ProfileData).then((result) => {

    loading.dismissAll();
    this.data = result;
    //let status_code = this.data.code;

    this.presentAlert('',this.data.message);

    this.navCtrl.pop()

  }, (err) => {
    loading.dismissAll();
    this.presentAlert("","Request not sent. Please try again");
  });

  }
  }

  presentAlert(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
  });
  alert.present();
  }

  getPhoto(){
          var buttons=[
            {
              text: 'Camera',
              handler: () => {
                this.takePhoto(this.camera.PictureSourceType.CAMERA);
              }
            },{
              text: 'Gallery',
              handler: () => {
                  this.takePhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
              }
            },{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {}
            }

          ]
          if(this.profilePic!=this.sanatizeBase64Image('assets/imgs/profile.png')){
            let butttondelete={text:'Delete',handler:()=>{this.delete_image()}}
            buttons.push(butttondelete)
          }
          this.actionSheetCtrl.create({buttons: buttons}).present();
      }

      delete_image(){
          this.profilePic=this.sanatizeBase64Image('assets/imgs/profile.png');
      }

      sanatizeBase64Image(image) {
      if(image) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(image);
      }
    }

      takePhoto(sourceType) {
      //console.log("coming here");

      const options: CameraOptions = {
        quality: 100,
        sourceType: sourceType,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
        //allowEdit:true
        // targetWidth: 300,
        // targetHeight: 300
      };

      this.camera.getPicture(options).then(
        imageData => {
          this.profilePic = this.sanatizeBase64Image("data:image/jpeg;base64," + imageData);//"data:image/jpeg;base64," + imageData;
          //this.base64Image = "data:image/jpeg;base64," + imageData;
          //this.photos.push(this.base64Image);
          //this.photos.reverse();
          this.sendData(imageData);
        },
        err => {
          console.log(err);
        }
      );
    }

    sendData(imageData) {
      //this.userData.imageB64 = imageData;

      let loading = this.loadingCtrl.create({});
      loading.present();

      this.memeservice.editPic(imageData).then(
        result => {
          loading.dismissAll();
          this.responseData = result;
          //let status_code = this.responseData.status_code;
          //let message = this.responseData.message;

          this.presentAlert('',this.responseData.message);

        },
        err => {
          // Error log
          loading.dismissAll();
          this.presentToast('Failed to upload your photo. Please try again');
        }
      );
    }

    presentToast(toastmessage) {
      let toast = this.toastCtrl.create({
        message: toastmessage,
        duration: 3000,
        position: 'bottom',
        cssClass: "toast-success"
      });

      toast.onDidDismiss(() => {
        //console.log('Dismissed toast');
      });

      toast.present();
    }

  dismiss() {
   this.viewCtrl.dismiss();
  }

}
