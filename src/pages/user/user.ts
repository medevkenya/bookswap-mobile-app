import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { MemeProvider } from '../../providers/meme/meme';
import { MemedetailsPage } from '../memedetails/memedetails';
/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  public memberSince;
  public fullName;
  public nickName;
  public profilePic;
  public count;
  public userId;

  list:Array<any>;

  loading:any;
  data: any;
  errorMessage: string;
  page = 1;
  order = 2;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  nodataavailable:boolean = false;

  bannerText:any;
  bannerImage:any;
  bannerUrl:any;
  showbanner:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private loadingCtrl:LoadingController,
    public memeservice: MemeProvider
  ) {
    this.memberSince = this.navParams.get('memberSince');
    this.fullName = this.navParams.get('fullName');
    this.nickName = this.navParams.get('nickName');
    this.count = this.navParams.get('count');
    this.profilePic = this.navParams.get('profilePic');
    this.userId = this.navParams.get('userId');
  }

  openDetails(event,listitem) {
   this.navCtrl.push(MemedetailsPage, {
   id: listitem.id,
   listitem:listitem
   });
  }

  doRefresh(refresher){
    this.page = 1;
    this.loadList();

     if(refresher)
        refresher.complete();
  }

  loadList() {
      if(navigator.onLine !== true) {
        console.log("not online")
        this.presentAlert('No Internet Connection!',"Please put data connection on and try again");
      }
      else {
        let loading = this.loadingCtrl.create({});
    loading.present();
    this.memeservice.getListByUserId(this.page,this.userId)
       .then(
         res => {
           loading.dismissAll();
           this.data = res;
           //console.log("getListByUserId --"+JSON.stringify(this.data));
           this.list = this.data.result.data;
           this.perPage = this.data.result.per_page;
           this.totalData = this.data.result.total;
           this.totalPage = this.data.result.total_pages;

           //Banner
           if(this.data.result.showbanner == true) {
             this.bannerText = this.data.result.bannerText;
             this.bannerImage = this.data.result.bannerImage;
             this.bannerUrl = this.data.result.bannerUrl;
             this.showbanner = true;
           }

           if(this.data.result.total == 0) {
             this.nodataavailable = true;
           }
           else {
             this.nodataavailable = false;
           }
         },
         error =>  this.errorMessage = <any>error);
  }
}

doInfinite(infiniteScroll) {
this.page = this.page+1;
setTimeout(() => {
  this.memeservice.getListByUserId(this.page,this.userId)
     .then(
       res => {
         this.data = res;
         this.perPage = this.data.result.per_page;
         this.totalData = this.data.result.total;
         this.totalPage = this.data.result.total_pages;
         for(let i=0; i<this.data.result.data.length; i++) {
           this.list.push(this.data.result.data[i]);
         }
       },
       error =>  this.errorMessage = <any>error);

  console.log('Async operation has ended');
  infiniteScroll.complete();
}, 1000);
}

presentAlert(alerttitle,alertcontent) {
const alert = this.alertCtrl.create({
title: alerttitle,
subTitle: alertcontent,
buttons: ['Ok']
});
alert.present();
//alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
}

ionViewDidLoad() {
  console.log('ionViewDidLoad UserPage');
}

ionViewDidEnter() {
  this.loadList();
}

openBanner() {
  if(this.bannerUrl != null) {
    window.open(this.bannerUrl, '_system');
  }
}

}
