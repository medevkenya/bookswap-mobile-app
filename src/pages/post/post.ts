import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DomSanitizer} from "@angular/platform-browser";
import {ActionSheetController, AlertController, Platform, ToastController, LoadingController, ModalController, App} from "ionic-angular";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {File,FileEntry} from "@ionic-native/file";
import {FilePath} from "@ionic-native/file-path";
//import { Chooser } from '@ionic-native/chooser';
//import { FileChooser } from '@ionic-native/file-chooser';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ServerUrlProvider } from '../../providers/server-url/server-url';
import { MemeProvider } from '../../providers/meme/meme';
//import { SigninPage } from '../signin/signin';
import { Profile } from '../profile/profile';
import { Storage } from '@ionic/storage';
//import {Base64} from "@ionic-native/base64";
//import { ImagePicker } from '@ionic-native/image-picker';
import { ImagePicker } from '@ionic-native/image-picker';

/**
 * Generated class for the PostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {

    server_url:string;

    imagesList = [];
    allimages:any;
    selectedImagesList = [];

    // public isUploading = false;
    // public uploadingProgress = {};
    // public uploadingHandler = {};
    public imageURI: any = [];
    public UriData: any = [];
    public imageURIData: any = [];
    // protected imagesValue: Array<any>;

    // public is = {};
    // ImageData:any;

    spinnerfront:boolean = false;
    hidephoto:boolean = false;
    img:any;

    public base64Image: string;
    public responseData: any;

    data:any;
    loading:any;

    PostData = { photo:'', appId:'' };

    TextData = { description:'' };

    segment: string;

    FileURI:any;
    FileName:any;

    imageDataFinal:any;
    fileNameFinal:any;

    arrayTypeVideos:any;
    arrayTypeImages:any;

    uploadPercentage:any;

  constructor(
    //private imagePicker: ImagePicker,
    private imagePicker: ImagePicker,
      private sanitization: DomSanitizer,
      private actionSheetCtrl: ActionSheetController,
      private camera: Camera,
      private transfer: FileTransfer,
      private file: File,
      private alertCtrl: AlertController,
      private toastCtrl: ToastController,
      private app:App,
      private platform: Platform,
      private filePath: FilePath,
      private zone: NgZone,
      public navCtrl: NavController,
      private loadingCtrl:LoadingController,
      public navParams: NavParams,
      //private fileChooser: FileChooser,
      //public base64: Base64,
      private serverUrl: ServerUrlProvider,
      public memeservice: MemeProvider,
      public modalCtrl: ModalController,
      public storage: Storage
    ) {

      // //check if is logged in
      //   this.storage.get('MemesUphasLoggedIn')
      //   .then((MemesUphasLoggedIn) => {
      //     if (MemesUphasLoggedIn == "true") {
      //
      //     }
      //     else {
      //       this.app.getRootNav().setRoot(SigninPage, {page:'PostPage'});
      //       // let modal = this.modalCtrl.create(SigninPage, {page:'PostPage'});
      //       // // modal.onDidDismiss(data => {
      //       // //   this.loadProfile();
      //       // // });
      //       // modal.present();
      //     }
      //   });
      //   //end check if logged in

    this.server_url = serverUrl.url;
    this.segment = "photos";
  }

  getPictures() {
  this.imagePicker.getPictures({
    maximumImagesCount: 15,
    outputType: 1
  }).then((results) => {
  //console.log('selectedImg--'+results);
    this.allimages = results;
    // this.images = selectedImg;
    // this.allimages.forEach(iselect =>
    //   this.imagesList.push("data:image/jpeg;base64," + iselect)
    //   //this.selectedImagesList.push(i)
    // );


    //for(var i in this.allimages){
    if(this.allimages != "OK") {
    for (var i = 0; i < results.length; i++) {
        //console.log(this.allimages[i]);//This will print the objects
        //console.log(i);//This will print the index of the objects
        this.imagesList.push("data:image/jpeg;base64," + results[i])
    }

    //this.selectedImagesList = this.selectedImagesList.push(i);
    this.PostData.photo = this.allimages;
    this.PostData.appId = localStorage.getItem("MemesasaaccessToken");

  }

  }, (err) => { });
}

//   selectMultipleImages() {
//     let multipleoptions = {
//               width: 600,
//               height: 400,
//               quality: 75,
//               outputType: 1
//             };
//     this.imagePicker.getPictures(multipleoptions).then((results) => {
//   for (var i = 0; i < results.length; i++) {
//       console.log('Image URI: ' + results[i]);
//   }
// }, (err) => { });
//   }

  segmentChanged(event): void {

  }

  selectedPhotos() {

  }

  selectedVideos() {

  }

  selectedText() {

  }

  getPhoto(){
        var buttons=[
          {
            text: 'From Camera',
            handler: () => {
              this.takePhoto(this.camera.PictureSourceType.CAMERA);
            }
          },{
            text: 'From Photo Gallery',
            handler: () => {
                //this.takePhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
                this.getPictures();
            }
          },{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {}
          }

        ]
        if(this.img!=this.sanatizeBase64Image('assets/imgs/meme.png')){
          let butttondelete={text:'Delete',handler:()=>{this.delete_image()}}
          buttons.push(butttondelete)
        }
        this.actionSheetCtrl.create({buttons: buttons}).present();
    }

    delete_image(){
        this.imagesList = [];
        this.img=this.sanatizeBase64Image('assets/imgs/meme.png');
    }

    sanatizeBase64Image(image) {
    if(image) { //console.log("sanatizeBase64Image---"+image);
       this.spinnerfront = false;
      return this.sanitization.bypassSecurityTrustResourceUrl(image);
    }
  }

  takePhoto(sourceType) {

    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
      //allowEdit:true
    };

    this.camera.getPicture(options).then(
      imageData => {

        this.spinnerfront = true;
        this.hidephoto = true;

        //console.log("camera getPicture-----"+imageData);
        this.img = this.sanatizeBase64Image("data:image/jpeg;base64," + imageData);
        //this.base64Image = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpeg;base64,'+imageData);//this.sanitizer.bypassSecurityTrustResourceUrl(imageData);//"data:image/jpeg;base64," + imageData;

        //this.photos.reverse();
        //this.sendData(imageData);
        this.PostData.photo = imageData;
      },
      err => {
        //console.log("getPicture---"+err);
      }
    );
  }

  sendData() {

  if(navigator.onLine !== true) {
    console.log("not online")
    this.presentAlert('No Internet Connection!',"Please put data connection on and try again");
  }
  else if(this.PostData.photo == "" || this.PostData.photo == null) {
    this.presentAlert('',"Please browse and select a valid photo");
  }
    else {

  let loading = this.loadingCtrl.create({});
  loading.present();

  this.PostData.appId = localStorage.getItem("MemesasaaccessToken");//this.memeservice.getToken();

  this.memeservice.uploadPhotos(this.PostData).then(
    result => {
      loading.dismissAll();
      this.hidephoto = false;
      this.responseData = result;
      let status_code = this.responseData.code;
      let message = this.responseData.message;
      //console.log("brian---photo responseData----"+JSON.stringify(this.responseData));
      if(status_code == 1) {
        this.delete_image();
        this.navCtrl.push(Profile);
      }

      this.presentToast(message);
    },
    err => {
      this.hidephoto = false;
      // Error log
      loading.dismissAll();
      this.presentToast('Failed to upload your photo. Please try again');
    }
  );
}
}

  //UPLOAD PHOTOS
  // asktouploadanimage() {
    //   const alert = this.alertCtrl.create({
    //     title: 'Image',
    //     subTitle: 'Please upload at least one image of your item',
    //     buttons: ['Ok']
    //   });
    //   alert.present();
    // }

//     public uploadImages(): Promise<Array<any>> {
//       console.log("step 1....")
//         return new Promise((resolve, reject) => {
//
//
//             console.log("count images in array---"+this.images.length);
//             if(this.images.length != 4) {
//               const alert = this.alertCtrl.create({
//                 title: 'Photos',
//                 subTitle: 'Please upload 4 photos of your product/service',
//                 buttons: ['Ok']
//               });
//               alert.present();
//               //asktouploadanimage();
//               return;
//             }
//
//             this.isUploading = true;
//
//             Promise.all(this.images.map(image => {
//               console.log("step 2....")
//                 return this.uploadImage(image);
//             }))
//                 .then(resolve)
//                 .catch(reason => {
//                     this.isUploading = false;
//                     this.uploadingProgress = {};
//                     this.uploadingHandler = {};
//                     reject(reason);
//                 });
//
//         });
//     }
//
//     public abort() {
//         if (!this.isUploading)
//             return;
//         this.isUploading = false;
//         for (let key in this.uploadingHandler) {
//             this.uploadingHandler[key].abort();
//         }
//     }
//
//     // ======================================================================
//
//     protected removeImage(image) {
//         if (this.isUploading)
//             return;
//         this.confirm("Are you sure to remove it?").then(value => {
//             if (value) {
//                 this.removeFromArray(this.imagesValue, image);
//                 this.removeFromArray(this.images, image.url);
//             }
//         });
//     }
//
//     protected showAddImage() {
//         if (!window['cordova']) {
//             let input = document.createElement('input');
//             input.type = 'file';
//             input.accept = "image/x-png,image/gif,image/jpeg";
//             input.click();
//             input.onchange = () => {
//               console.log("1 shit happens here---");
//                 let blob = window.URL.createObjectURL(input.files[0]);
//                 //this.images.push(blob);
//                 //this.util.trustImages();
//
//
//
//                 // Special handling for Android library  //
//                                       if (this.platform.is('ios')) {
//                                         this.ImageData = blob.replace(/^file:\/\//, '');
//                                       }
//                                       else {
//                                         this.ImageData = blob;
//                                       }
//                                       this.images.push(this.ImageData); //if you have to show multiple image
//                                       //this.photos.reverse();
//                                       this.trustImages();
//
//
//             }
//         } else {
//
//             new Promise((resolve, reject) => {
//                 let actionSheet = this.actionSheetCtrl.create({
//                     title: 'Add a photo',
//                     buttons: [
//                         {
//                             text: 'From photo library',
//                             handler: () => {
//                                 resolve(this.camera.PictureSourceType.PHOTOLIBRARY);
//                             }
//                         },
//                         {
//                             text: 'From camera',
//                             handler: () => {
//                                 resolve(this.camera.PictureSourceType.CAMERA);
//                             }
//                         },
//                         {
//                             text: 'Cancel',
//                             role: 'cancel',
//                             handler: () => {
//                                 reject();
//                             }
//                         }
//                     ]
//                 });
//                 actionSheet.present();
//             }).then(sourceType => {
//                 if (!window['cordova'])
//                     return;
//                 let options: CameraOptions = {
//                     quality: 100,
//                     allowEdit: true,
//                     sourceType: sourceType as number,
//                     saveToPhotoAlbum: false,
//                     correctOrientation: true,
//                     //encodingType: this.camera.EncodingType.JPEG,
//                     //destinationType: this.camera.DestinationType.FILE_URI
//                 };
//
//
//                 // this.camera.getPicture(options).then((imagePath) => {
//                 //     this.images.push(imagePath);
//                 //     this.trustImages();
//                 // });
//
//                 // Get the data of an image
//                 this.camera.getPicture(options).then((imagePath) => {
//                   // Special handling for Android library
//                   if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
//                     this.filePath.resolveNativePath(imagePath)
//                       .then(filePath => {
//                         let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
//                         //let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
//                         //this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
//                              this.images.push(correctPath);
//                              this.trustImages();
//                       });
//                   } else {
//                     //var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
//                     var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
//                     //this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
//                     this.images.push(correctPath);
//                     this.trustImages();
//                   }
//                 }, (err) => {
//                   this.presentToast("Error while selecting image.");
//                 });
//
//
//                 // this.camera.getPicture(options).then((imagePath) => {
//                 //
//                 //   const alert = this.alertCtrl.create({
//                 //    title: "Test",
//                 //    subTitle: "check me----"+imagePath,
//                 //    buttons: ['Ok']
//                 //   });
//                 //   alert.present();
//                 //
//                 //                       // Special handling for Android library  //
//                 //                       if (this.platform.is('ios')) {
//                 //                         this.ImageData = imagePath.replace(/^file:\/\//, '');
//                 //                       }
//                 //                       else {
//                 //                         this.ImageData = imagePath;
//                 //                       }
//                 //                       this.images.push(this.ImageData);
//                 //                       this.trustImages();
//                 //                       //this.photos.push(this.ImageData); //if you have to show multiple image
//                 //                       //this.photos.reverse();
//                 //             });
//
//
//             }).catch((error) => {
//
//
//               const alert = this.alertCtrl.create({
//                title: "error",
//                subTitle: "check error----"+error,
//                buttons: ['Ok']
//               });
//               alert.present();
//
//             });
//         }
//     }
//
//
//
//     // Create a new name for the image
// // private createFileName() {
// //   var d = new Date(),
// //   n = d.getTime(),
// //   newFileName =  n + ".jpg";
// //   return newFileName;
// // }
//
// // Copy the image to a local folder
// // private copyFileToLocalDir(namePath, currentName, newFileName) {
// //   this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
// //     this.images.push(namePath);
// //     this.trustImages();
// //     //this.lastImage = newFileName;
// //   }, error => {
// //     this.presentToast('Error while storing file.');
// //   });
// // }
//
//
//     private uploadImage(targetPath) {
//       console.log("step 3...."+targetPath)
//         return new Promise((resolve, reject) => {
//             this.uploadingProgress[targetPath] = 0;
//
//             if (window['cordova']) {
//               console.log("window is cordova");
//                 let options = {
//                     fileKey: "files[]",
//                     fileName: targetPath,
//                     chunkedMode: false,
//                     mimeType: "multipart/form-data",
//                 };
//
//                 const fileTransfer = new FileTransferObject();
//                 this.uploadingHandler[targetPath] = fileTransfer;
//
//                 console.log("step 4....url-"+this.server_url)
//
//                 fileTransfer.upload(targetPath, this.server_url, options).then(data => {
//                   console.log("step 5....Response---"+JSON.stringify(data.response));
//                     resolve(JSON.parse(data.response));
//                 }).catch(() => {
//                     askRetry();
//                 });
//
//                 fileTransfer.onProgress(event2 => {
//                     this.uploadingProgress[targetPath] = event2.loaded * 100 / event2.total;
//
//                     // if(this.uploadingProgress[targetPath] == 100) {
//                     //   console.log("success 100....")
//                     // }
//
//                 });
//             } else {
//               console.log("Not cordova");
//                 const xhr = new XMLHttpRequest();
//                 xhr.open('GET', targetPath, true);
//                 xhr.responseType = 'blob';
//                 xhr.onload = (e) => {
//                     if (xhr['status'] != 200) {
//                       console.log("blob API not supported");
//                         this.presentToast("Your browser doesn't support blob API");
//                         console.error(e, xhr);
//                         askRetry();
//                     } else {
//                       console.log("blob xhr response");
//                         const blob = xhr['response'];
//                         let formData: FormData = new FormData(),
//                             xhr2: XMLHttpRequest = new XMLHttpRequest();
//                         formData.append('image', blob);
//
//                         formData.append('userId', localStorage.getItem("FarmcustomeruserId"));
//                         formData.append('id', localStorage.getItem("FarmMarketItemid"));
//                         this.uploadingHandler[targetPath] = xhr2;
//
//                         xhr2.onreadystatechange = () => {
//                           console.log("onreadystatechange 555545454")
//                             if (xhr2.readyState === 4) {
//                               console.log("step 333333----status---"+xhr2.status);
//                                 if (xhr2.status == 200) {
//
//                                   let jsonRes = JSON.parse(xhr2.response);
//                                 console.log("step 777----200---"+jsonRes.code);
//
//                                 /////////////////////////////
//                                 if(jsonRes.code == 1) {
//                                   //this.openHome();
//                                 }
//                                 ////////////////////////////
//
//                                     resolve(JSON.parse(xhr2.response));
//                               }  else {
//                                 console.log("step 999999----"+xhr2.status)
//                                     askRetry();
//                                   }
//                             }
//                             else {
//                               console.log("step 5555")
//                             }
//                         };
//
//   xhr2.onerror = function () {
//     console.log("step error---"+xhr2.status)
//     //error(xhr, xhr.status);
//   };
//
//                         xhr2.upload.onprogress = (event) => {
//                             this.uploadingProgress[targetPath] = event.loaded * 100 / event.total;
//                         };
//
//                         // if(this.uploadingProgress[targetPath] == 100) {
//                         //   console.log("success 200....")
//                         // }
//
//                         console.log("step 6 Posting....url-"+this.server_url)
//                         xhr2.open('POST', this.server_url, true);
//                         xhr2.send(formData);
//                     }
//                 };
//                 xhr.send();
//             }
//
//             let askRetry = () => {
//                 // might have been aborted//
//                 if (!this.isUploading) return reject(null);
//                 this.confirm('Do you wish to retry?', 'Upload failed').then(res => {
//                     if (!res) {
//                         this.isUploading = false;
//                         for (let key in this.uploadingHandler) {
//                             this.uploadingHandler[key].abort();
//                         }
//                         return reject(null);
//                     }
//                     else {
//                         if (!this.isUploading) return reject(null);
//                         this.uploadImage(targetPath).then(resolve, reject);
//                     }
//                 });
//             };
//         });
//     }
//
//     removeFromArray<T>(array: Array<T>, item: T) {
//                 let index: number = array.indexOf(item);
//                 if (index !== -1) {
//                     array.splice(index, 1);
//                 }
//             }
//
//     trustImages() {
//         this.imagesValue = this.images.map(
//             val => {
//                 return {
//                     url: val,
//                     sanitized: this.sanitization.bypassSecurityTrustStyle("url(" + val + ")")
//                 }
//             }
//         );
//     }
//
//     confirm(text, title = '', yes = "Yes", no = "No") {
//                 return new Promise(
//                     (resolve) => {
//                         this.alertCtrl.create({
//                             title: title,
//                             message: text,
//                             buttons: [
//                                 {
//                                     text: no,
//                                     role: 'cancel',
//                                     handler: () => {
//                                         //resolve(false);
//                                     }
//                                 },
//                                 {
//                                     text: yes,
//                                     handler: () => {
//                                         resolve(true);
//                                     }
//                                 }
//                             ]
//                         }).present();
//                     }
//                 );
//             }
//
    presentToast(text) {
      let toast = this.toastCtrl.create({
        message: text,
        duration: 3000,
        position: 'top'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
    }
//   //UPLOAD PHOTOS

  //UPLOAD VIDEO
// uploadVideo()
// {
//     this.fileChooser.open()
//     .then(uri =>
//     {
//       console.log(uri)
//        const fileTransfer: FileTransferObject = this.transfer.create();
//
//
//        let appId = this.memeservice.getToken();
//   // regarding detailed description of this you cn just refere ionic 2 transfer plugin in official website
//     let options1: FileUploadOptions = {
//        fileKey: 'file_upload_file',
//        //fileName: 'name.pdf',
//        headers: {},
//        params: {"app_key":"Testappkey","appId":appId},
//        chunkedMode : false
//
//     }
//
//     let loading = this.loadingCtrl.create({ content: 'Please wait...' });
//     loading.present();
//
//     fileTransfer.upload(uri, ''+this.server_url+'uploadVideo', options1)
//      .then((data) => {
//      // success
//      loading.dismissAll();
//      this.resp = data;
//
//        console.log("success"+JSON.stringify(data));
//        this.presentAlert("",this.resp.response);
//
//      }, (err) => {
//      // error
//      loading.dismissAll();
//      this.presentAlert("","Upload failed, please try again");
//      //console.log("error"+JSON.stringify(err));
//          });
//
//     })
//     .catch(e => console.log(e));
// }
// //https://stackoverflow.com/questions/42784079/how-to-add-a-progress-bar-on-uploading-a-file
// uploadVideo()
// {
//     this.fileChooser.open()
//     .then(uri =>
//     {
//       console.log("uploadVideo uri =="+uri)
//        const fileTransfer: FileTransferObject = this.transfer.create();
//
//        let appId = this.memeservice.getToken();
//   // regarding detailed description of this you cn just refere ionic 2 transfer plugin in official website
//     let options1: FileUploadOptions = {
//        fileKey: 'file_upload_file',
//        //fileName: 'name.pdf',
//        headers: {},
//        params: {"app_key":"Testappkey","appId":appId},
//        chunkedMode : false
//
//     }
//
//     let loading = this.loadingCtrl.create({ content: 'Uploading...' });
//     loading.present();
//
//     //**Progress Bar**
//   fileTransfer.onProgress((e)=>
//   {
//
//     if (e) {
//             console.log("progress success =====")
//         } else {
//             console.log("progress error =====")
//         }
//
//     let prg=(e.lengthComputable) ?  Math.round(e.loaded / e.total * 100) : -1;
//     console.log("video upload progress:"+prg);
//   });
//
//     fileTransfer.upload(uri, ''+this.server_url+'uploadVideo', options1)
//      .then((data) => {
//      // success
//      loading.dismissAll();
//      this.data = data;
//
//        console.log("success"+JSON.stringify(data));
//        this.presentAlert("",this.data.response);
//
//      }, (err) => {
//      // error
//      loading.dismissAll();
//      console.log("error"+JSON.stringify(err));
//      this.presentAlert("","Upload failed, please try again");
//          });
//
//     })
//     .catch(e => console.log('file chosee err = '+e));
// }
//   //UPLOAD VIDEO

  // ChooseFile(){
  //   //var filter = { "mime": "application/pdf" }  // text/plain, image/png, image/jpeg, audio/wav etc
  //   // this.fileChooser.open(filter, function(response){
  //   //   //success
  //   //   console.log("success"+reponse);
  //   // }, function(error){
  //   //   console.log("error"+error);
  //   // })
  //   //get file with cordova file chooser
  //   this.fileChooser.open()
  //     .then(uri => {
  //       //file uri for upload
  //       this.FileURI=uri;
  //       this.filePath.resolveNativePath(uri) //get file path
  //       .then(filePath => {
  //         this.file.resolveLocalFilesystemUrl(filePath).then(fileInfo => //get info file
  //           {
  //             let files = fileInfo as FileEntry;
  //             files.file(success =>
  //               {
  //                 this.zone.run(() => { //updating binding file name
  //                   this.FileName=success.name; //get file name
  //                   console.log('FileName choosen---'+this.FileName);
  //                 });
  //               });
  //           },err =>
  //           {
  //             console.log('ChooseFile 1 --'+err);
  //             throw err;
  //           });
  //       });
  //     }).catch(e => console.log('ChooseFile 2 --'+e));
  // }

//https://stackoverflow.com/questions/46967119/how-to-get-file-name-and-mime-type-before-upload-in-ionic2

  uploadDoc() {
    //show loading spinner
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();

    const fileTransfer: FileTransferObject = this.transfer.create();
    //Local PC Web service if using an emulator
    let URL=''+this.server_url+'uploadVideo';

    //Local PC Web service if using the real device and in one network
    //let URL="http://192.168.99.187/upload-file-ionic/upload.php";

    //console.log('extension==='+this.get_url_extension(this.fileNameFinal));

    //option upload
    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: this.fileNameFinal,
      chunkedMode: false,
     params: {'appId':localStorage.getItem("MemesasaaccessToken"),'fileName':this.fileNameFinal,'extension':this.get_url_extension(this.fileNameFinal)}, // add another parameter (opsional)
      headers: {}
    }

    //**Progress Bar**
      fileTransfer.onProgress((e)=>
      {

        if (e) {
                console.log("progress success =====")
            } else {
                console.log("progress error =====")
            }

        let prg=(e.lengthComputable) ?  Math.round(e.loaded / e.total * 100) : -1;
        console.log("video upload progress:"+prg);
        //this.uploadPercentage = prg+'%';
        //loader.setContent(prg);
      });

    fileTransfer.upload(this.imageDataFinal, URL, options)
      .then((result) => {
      //console.log(data);
      loader.dismiss();
      this.data = result;

      if(JSON.parse(this.data.response).code == 1) {
        this.deleteFile(1);
      }

      this.presentToast(JSON.parse(this.data.response).message);
    }, (err) => {
    //console.log(err);
      loader.dismiss();
      this.presentToast("Request not sent. Please try again");
    });
  }

  deleteFile(docsi) {
    this.UriData = [];
  }

//   selectDocument() {
//
//     this.arrayTypeVideos = ["mp4", "mov", "wmv", "flv", "avi", "webm", "mkv"];
//     this.arrayTypeImages = ["jpg", "jpeg", "png", "gif"];
// //
//     //var filter = { "mime": "video/*" }  // text/plain, image/png, image/jpeg, audio/wav etc
//
//        this.fileChooser.open({ "mime": "video/*" }).then((imageData) => {
//             console.log('imagedata---',imageData);
//
//             this.imageURIData.push(imageData);
//
//             //this.UriData.push({name:imageData.substr(imageData.lastIndexOf('/') + 1)});
//
//             console.log("Data Uri =>", this.UriData);
//
//             //file uri for upload
//             this.FileURI=imageData;
//             this.filePath.resolveNativePath(imageData) //get file path
//             .then(filePath => {
//               this.file.resolveLocalFilesystemUrl(filePath).then(fileInfo => //get info file
//                 {
//                   let files = fileInfo as FileEntry;
//                   files.file(success =>
//                     {
//                       this.zone.run(() => { //updating binding file name
//                         this.FileName=success.name; //get file name
//                         //console.log('FileName choosen---'+this.FileName);
//
//                         //let photoFile = this.sanitization.bypassSecurityTrustResourceUrl(imageData);
//                         //bypassSecurityTrustResourceUrl;
//
//                         this.imageDataFinal = imageData;
//                         this.fileNameFinal = success.name;
//                         //console.log('FileName choosen extension---'+this.get_url_extension(this.fileNameFinal));
//
//                         let fileExtension = this.get_url_extension(this.fileNameFinal);
//
//                         if(this.arrayTypeVideos.indexOf(fileExtension) > -1)
//                         {
//                          //console.log("accepted type");
//                          this.UriData.push({'photoFile':'','fileType':'video','extension':fileExtension,'originalFileName':success.name,name:imageData.substr(imageData.lastIndexOf('/') + 1)});
//                         }
//                         else if(this.arrayTypeImages.indexOf(fileExtension) > -1)
//                         {
//                          //console.log("accepted type");
//                          let photoFile = 0;//this.sanatizeBase64Image("data:image/jpeg;base64," + imageData);
//                          //console.log("photoFile-----"+photoFile);
//                          this.UriData.push({'photoFile':photoFile,'fileType':'image','extension':fileExtension,'originalFileName':success.name,name:imageData.substr(imageData.lastIndexOf('/') + 1)});
//                         }
//                         else {
//                           this.presentToast("Files with extension ("+fileExtension+") are not supported");
//                         }
//
//                         //this.uploadDoc();
//                       });
//                     });
//                 },err =>
//                 {
//                   //console.log('ChooseFile 1 --'+err);
//                   throw err;
//                 });
//             });
//
//             // //show loading spinner
//             // let loader = this.loadingCtrl.create({
//             //   content: "Uploading..."
//             // });
//             // loader.present();
//             //
//             // const fileTransfer: FileTransferObject = this.transfer.create();
//             // //Local PC Web service if using an emulator
//             // let URL=''+this.server_url+'uploadVideo';
//             //
//             // //option upload
//             // let options: FileUploadOptions = {
//             //   fileKey: 'file',
//             //   fileName: this.FileName,
//             //   chunkedMode: false,
//             //  params: {'appId':localStorage.getItem("MemesasaaccessToken")}, // add another parameter (opsional)
//             //   headers: {}
//             // }
//             // fileTransfer.upload(imageData, URL, options)
//             //   .then((data) => {
//             //   console.log(data);
//             //   loader.dismiss();
//             //   this.presentToast("File uploaded successfully");
//             // }, (err) => {
//             //   console.log(err);
//             //   loader.dismiss();
//             //   this.presentToast("Error");
//             // });
//
//             // //let filePath: string = 'file:///...';
//             // this.base64.encodeFile(imageData).then((base64File: string) => {
//             //   console.log('this.base64--succ-'+base64File);
//             // }, (err) => {
//             //   console.log('this.base64-err--'+err);
//             // });
//
//          //    this.base64.encodeFile(imageData).then((base64File:string) => {
//          //      console.log('base64File---'+base64File);
//          //  this.imageURI.push(base64File.substr(base64File.indexOf(',') + 1));
//          //
//          //  console.log('this.imageURI---'+this.imageURI);
//          //
//          // }, (err) => {
//          //     console.log('base64 encodeFile---'+err);
//          // });
//         }, (err) => {
//             console.log('fileChooser---'+err);
//             alert(err);
//         });
//     }

  get_url_extension(url) {
    return url.split(/[#?]/)[0].split('.').pop().trim();
}

    // uploadDoc() {
    //   if(navigator.onLine !== true) {
    //     console.log("not online")
    //     this.presentAlert('Internet Connection',"Please put data connection on and try again");
    //   }
    //   else {
    //
    //   let loading = this.loadingCtrl.create({ content: 'Uploading...' });
    //   loading.present();
    //
    //     var data = {
    //         access_token: "123456",//this.access_token,
    //         documents: this.FileURI,
    //         appId: localStorage.getItem("MemesasaaccessToken")
    //     }
    //     this.memeservice.uploadVideo(data).then(
    //         data => {
    //           loading.dismissAll();
    //             this.data = data;
    //             let toast = this.toastCtrl.create({
    //                 message:  this.data.message,
    //                 duration: 3000,
    //                 position: 'top'
    //             });
    //             toast.present();
    //             //this.navCtrl.setRoot(PageName);
    //         }, (err) => {
    //           loading.dismissAll();
    //           this.presentAlert("","Request not sent. Please try again");
    //             console.log(err);
    //         });
    //       }
    // }

  //SEND TEXT
  sendText() {

  if(navigator.onLine !== true) {
    console.log("not online")
    this.presentAlert('Internet Connection',"Please put data connection on and try again");
  }
  else if (this.TextData.description === "") {
    this.presentAlert('',"Enter a valid meme text to continue");
  }
  else {

  let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
  loading.present();

  this.memeservice.sendMemeText(this.TextData).then((result) => {

    loading.dismissAll();
    this.data = result;
    console.log("sendMemeText==="+JSON.stringify(this.data));

    if(this.data.code == 1) {
      this.TextData.description = "";
    }

    this.presentAlert('',this.data.message);

  }, (err) => {
    loading.dismissAll();
    this.presentAlert("","Request not sent. Please try again");
  });

  }
  }
  //SEND TEXT

  presentAlert(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
  title: alerttitle,
  subTitle: alertcontent,
  buttons: ['Ok']
  });
  alert.present();
  //alert.onDidDismiss(() => {  setTimeout(() => this.loadProfile(), 500); });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PostPage');
  }

}
