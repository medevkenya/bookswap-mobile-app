import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, App} from 'ionic-angular';
import { SigninPage } from '../signin/signin';
import { Profile } from '../profile/profile';
import { MemeProvider } from '../../providers/meme/meme';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

   loading: any;
   RegisterData = { fullName:'', nickName:'', email:'' };
   data: any;

  constructor(
    private loadingCtrl:LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public memeservice: MemeProvider,
    private app: App,
    public storage: Storage
  ) {

  }

  hideShowPassword() {
   this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
   this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
 }

  ActionRegister() {

    //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if(navigator.onLine === false) {
      this.presentAlert('Internet Connection',"Please put data connection on and try again");
  }
  else if (this.RegisterData.fullName === "") {
    this.presentAlert('',"Please enter your full name");
      }
      else if (this.RegisterData.nickName === "") {
        this.presentAlert('',"Please enter your nickname");
          }
  else if (this.RegisterData.email === "") {
    this.presentAlert('',"Please enter your Username (Phone No. / Email Address)");
      }
  //     else if(!re.test(this.RegisterData.email)) {
  //   this.presentAlert('',"Invalid email address");
  // }
  // else if (this.RegisterData.password === "") {
  //   this.presentAlert('',"Please enter your password");
  //     }
  else {

    let loading = this.loadingCtrl.create({});
  loading.present();

  this.memeservice.Register(this.RegisterData).then((result) => {
    loading.dismissAll();
    this.data = result;
    //localStorage.setItem('token', this.data.access_token);
    //console.log("brian register-----"+JSON.stringify(this.data));
    let code = this.data.code;
    let message = this.data.message;

    if(code==1) {

      // this.app.getRootNav().setRoot(SigninPage);

      this.storage.set('MemesUphasLoggedIn', 'true');
      localStorage.setItem("MemesasaaccessToken",this.data.result.appId);
      this.storage.set('MemesasaaccessToken', this.data.result.appId);

      this.app.getRootNav().setRoot(Profile,{flag:'1'});

      //this.viewCtrl.dismiss();

    }

    this.presentAlert("",message);


  }, (err) => {
    loading.dismissAll();
    this.presentAlert("",err.error.message);
  });

  }
  }

  presentAlert(alerttitle,alertcontent) {
   const alert = this.alertCtrl.create({
     title: alerttitle,
     subTitle: alertcontent,
     buttons: ['Ok']
   });
   alert.present();
  }

  openSignIn() {
    this.navCtrl.push(SigninPage);
  }

  openHome() {
  //this.app.getRootNav().setRoot(TabsPage);
  }

  openTerms() { this.navCtrl.push('TermsPage');
    // if(this.activeLang == "zh" || this.activeLang == "zh-TW") {
    //   window.open('https://docsun.health/privacy-policy/','_system', 'location=yes');
    // }
    // else {
    //   window.open('https://docsun.health/en/privacy-policy/','_system', 'location=yes');
    // }
    // return false;
  }

}
