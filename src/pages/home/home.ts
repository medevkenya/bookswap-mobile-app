import { Component, ViewChild } from '@angular/core';
import { App, NavController, Content, PopoverController, AlertController, LoadingController } from 'ionic-angular';
import { PostPopover } from './post-popover';
import { Messages } from '../messages/messages';
import { MemeProvider } from '../../providers/meme/meme';
//import videojs from 'video.js';
import {DomSanitizer} from "@angular/platform-browser";
import { MemedetailsPage } from '../memedetails/memedetails';
import { PopularPage } from '../popular/popular';
import { UserPage } from '../user/user';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class Home {
  @ViewChild(Content) content: Content;

  public like_btn = {
    color: 'black',
    icon_name: 'heart-outline'
  };

  public tap: number = 0;

  list:Array<any>;

  loading:any;
  data: any;
  errorMessage: string;
  page = 1;
  order = 2;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  nodataavailable:boolean = false;

  profiles:any;
  todaylist:any;

  bannerText:any;
  bannerImage:any;
  bannerUrl:any;
  showbanner:boolean = false;

  notifyMe:any;
  label1:any;
  label2:any;

  constructor(
    private sanitization: DomSanitizer,
    public alertCtrl: AlertController,
    private loadingCtrl:LoadingController,
    public memeservice: MemeProvider,
    public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    public app: App
  ) {

    this.list = [
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'}
    ];

    this.todaylist = [
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'},
      {'id':0,'file':'../assets/imgs/placeholder.png','uploadType':'Category'}
    ];

    this.profiles = [
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'},
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'},
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'},
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'},
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'},
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'},
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'},
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'},
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'},
      {'id':0,'profilePic':'../assets/imgs/placeholder.png','nickName':'MemeLord'}
    ];



  }

  openBanner() {
    if(this.bannerUrl != null) {
      window.open(this.bannerUrl, '_system');
    }
  }

  sanatizeBase64Image(image) {
    return this.sanitization.bypassSecurityTrustResourceUrl(image);
  }

  openDetails(event,listitem) {
   this.navCtrl.push(MemedetailsPage, {
   id: listitem.id,
   listitem:listitem
   });
  }

  openPopular() {
   this.navCtrl.push(PopularPage, {});
  }

  shareItem() {
    this.memeservice.shareApp();
  }

  loadUsers(){
   this.memeservice.topProfiles()
   .then(result => {
     this.data = result;
     this.profiles = this.data.result;
   });
 }

 loadTodayList(){
  this.memeservice.todayList()
  .then(result => {
    this.data = result;
    this.todaylist = this.data.result;
  });
}

  doRefresh(refresher){
    this.page = 1;
    this.loadUsers();
    this.loadTodayList();
    this.loadList();
     if(refresher)
        refresher.complete();
  }

    loadList() {
        if(navigator.onLine !== true) {
          this.presentAlert('No Internet Connection!',"Please put data connection on and try again");
        }
        else {

          let loading = this.loadingCtrl.create({});
          loading.present();
          this.memeservice.topList(this.page)
         .then(
           res => {
             loading.dismissAll();
             this.data = res;

             this.list = this.data.result.data;
             this.perPage = this.data.result.per_page;
             this.totalData = this.data.result.total;
             this.totalPage = this.data.result.total_pages;

             this.notifyMe = this.data.result.notifyMe;
             this.label1 = this.data.result.label1;
             this.label2 = this.data.result.label2;

             //Banner
             if(this.data.result.showbanner == true) {
               this.bannerText = this.data.result.bannerText;
               this.bannerImage = this.data.result.bannerImage;
               this.bannerUrl = this.data.result.bannerUrl;
               this.showbanner = true;
             }

             if(this.data.result.total == 0) {
               this.nodataavailable = true;
             }
             else {
               this.nodataavailable = false;
             }
           },
           error =>  this.errorMessage = <any>error);
    }
  }

  doInfinite(infiniteScroll) {
  this.page = this.page+1;
  setTimeout(() => {
    this.memeservice.topList(this.page)
       .then(
         res => {
           this.data = res;
           this.perPage = this.data.result.per_page;
           this.totalData = this.data.result.total;
           this.totalPage = this.data.result.total_pages;
           for(let i=0; i<this.data.result.data.length; i++) {
             this.list.push(this.data.result.data[i]);
           }
         },
         error =>  this.errorMessage = <any>error);

    console.log('Async operation has ended');
    infiniteScroll.complete();
  }, 1000);
  }

  presentAlert(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
  title: alerttitle,
  subTitle: alertcontent,
  buttons: ['Ok']
  });
  alert.present();
  //alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  ionViewDidEnter() {
    this.loadUsers();
    this.loadTodayList();
    this.loadList();
  }

  likeButton() {
    if(this.like_btn.icon_name === 'heart-outline') {
      this.like_btn.icon_name = 'heart';
      this.like_btn.color = 'secondary';
      // Do some API job in here for real!
    }
    else {
      this.like_btn.icon_name = 'heart-outline';
      this.like_btn.color = 'black';
    }
  }

  tapPhotoLike(times) { // If we click double times, it will trigger like the post
    this.tap++;
    if(this.tap % 2 === 0) {
      this.likeButton();
    }
  }

  presentPostPopover() {
    let popover = this.popoverCtrl.create(PostPopover);
    popover.present();
  }

  goMessages(id) {
    this.app.getRootNav().push(Messages,{
      id:id
    });
  }

  open(userId,fullName,nickName,profilePic,count,memberSince) {
    this.navCtrl.push(UserPage,{
      userId:userId,
      fullName:fullName,
      nickName:nickName,
      profilePic:profilePic,
      count:count,
      memberSince:memberSince
    });
  }

  toggleNotify(notifyMe) {

    let loading = this.loadingCtrl.create({ content: 'Please Wait...' });
    loading.present();

    this.memeservice.toggleNotify(notifyMe).then((result) => {

      loading.dismissAll();
      this.data = result;

      if(this.data.code==1) {
        this.notifyMe = notifyMe;
      }

      if(notifyMe == 1) {
        this.presentAlert("","You will be notified everytime a new meme is posted");
      }
      else {
        this.presentAlert("","You will nolonger receive notifications when new memes are posted");
      }

    }, (err) => {
      loading.dismissAll();
      this.presentAlert("","Request not sent. Please try again");
    });

  }

  // swipePage(event) {
  //   if(event.direction === 1) { // Swipe Left
  //     console.log("Swap Camera");
  //   }
  //
  //   if(event.direction === 2) { // Swipe Right
  //     this.goMessages();
  //   }
  //
  // }
  //
  // scrollToTop() {
  //   this.content.scrollToTop();
  // }

}
