import { Component } from '@angular/core';
import { App, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';
import { MessageDetail } from '../message-detail/message-detail';
import { NewMessage } from '../new-message/new-message';
import { MemeProvider } from '../../providers/meme/meme';

@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class Messages {

  list:Array<any>;

  loading:any;
  data: any;
  errorMessage: string;
  page = 1;
  order = 2;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  nodataavailable:boolean = false;

  public id;

  MessageData = { comment:''};

  constructor(
    private loadingCtrl:LoadingController,
    public alertCtrl: AlertController,
    public memeservice: MemeProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    private app: App,
    private toastCtrl: ToastController
  ) {
    this.id = this.navParams.get('id');
  }

  doRefresh(refresher){
    this.loadList();

     if(refresher)
        refresher.complete();
  }

  loadList() {
      if(navigator.onLine !== true) {
        console.log("not online")
        this.presentAlert('No Internet Connection!',"Please put data connection on and try again");
      }
      else {
        let loading = this.loadingCtrl.create({ duration: 5000, content: 'Loading...' });
    loading.present();
    this.memeservice.getComments(this.page,this.id)
       .then(
         res => {
           loading.dismissAll();
           this.data = res;
           console.log("comments --"+JSON.stringify(this.data));
           this.list = this.data.result.data;
           this.perPage = this.data.result.per_page;
           this.totalData = this.data.result.total;
           this.totalPage = this.data.result.total_pages;
           if(this.data.result.total == 0) {
             this.nodataavailable = true;
           }
           else {
             this.nodataavailable = false;
           }
         },
         error =>  this.errorMessage = <any>error);
  }
}

doInfinite(infiniteScroll) {
this.page = this.page+1;
setTimeout(() => {
  this.memeservice.getComments(this.page,this.id)
     .then(
       res => {
         this.data = res;
         this.perPage = this.data.result.per_page;
         this.totalData = this.data.result.total;
         this.totalPage = this.data.result.total_pages;
         for(let i=0; i<this.data.result.data.length; i++) {
           this.list.push(this.data.result.data[i]);
         }
       },
       error =>  this.errorMessage = <any>error);

  console.log('Async operation has ended');
  infiniteScroll.complete();
}, 1000);
}

ActionPost() {
if(navigator.onLine !== true) {
  console.log("not online")
  this.presentAlert('Internet Connection',"Please put data connection on and try again");
}
else if (this.MessageData.comment === "") {
  this.presentToast("Enter a valid comment");
}
else {

let loading = this.loadingCtrl.create({ content: 'posting...' });
loading.present();

this.memeservice.postComment(this.MessageData.comment,this.id).then((result) => {

  loading.dismissAll();
  this.data = result;
  console.log("postComment==="+JSON.stringify(this.data));

  if(this.data.code == 1) {
    this.MessageData.comment = "";
    this.loadList();
  }

  this.presentToast(this.data.message);

}, (err) => {
  loading.dismissAll();
  this.presentToast("Request not sent. Please try again");
});

}
}

presentToast(text) {
  let toast = this.toastCtrl.create({
    message: text,
    duration: 3000,
    position: 'top'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
}

presentAlert(alerttitle,alertcontent) {
const alert = this.alertCtrl.create({
title: alerttitle,
subTitle: alertcontent,
buttons: ['Ok']
});
alert.present();
//alert.onDidDismiss(() => {  setTimeout(() => this.navCtrl.setRoot(TabsPage), 500); });
}

  ionViewDidLoad() {
    this.loadList();
  }

  goNewMessage() {
    this.app.getRootNav().push(NewMessage,{
      id:this.id
    });
  }

  goMessageDetail(sender:string, profile_img:string, last_message:string) {
    this.app.getRootNav().push(MessageDetail, { sender: sender, profile_img: profile_img, last_message: last_message});
  }

}
