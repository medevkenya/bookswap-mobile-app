import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, App } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { MemeProvider } from '../../providers/meme/meme';
import { ServerUrlProvider } from '../../providers/server-url/server-url';
import { Storage } from '@ionic/storage';
//import { TabsPage } from '../tabs/tabs';
import { Profile } from '../profile/profile';
//import { PostPage } from '../post/post';

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

   loading: any;
   LoginData = { email:'' };
   data: any;

   intendedPage:any;

   currentVersion:string;

  constructor(
    public storage: Storage,
    private loadingCtrl:LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public memeservice: MemeProvider,
    private app: App,
    private serverUrl: ServerUrlProvider
  ) {
    this.currentVersion = serverUrl.currentVersion;
    this.intendedPage = this.navParams.get('page');
  }

  hideShowPassword() {
   this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
   this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
 }

  openHome() {
    //this.app.getRootNav().setRoot(TabsPage);
  }

  ActionLogin() {

    //var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if(navigator.onLine === false) {
      this.presentAlert('Internet Connection',"Please put data connection on and try again");
  }
  else if (this.LoginData.email === "") {
    this.presentAlert('',"Please enter your Username (Phone No. / Email Address)");
  }
  // else if(!re.test(this.LoginData.email)) {
  //   this.presentAlert('',"Invalid email address");
  // }
  // else if (this.LoginData.password === "")
  // {
  //   this.presentAlert('',"Please enter your password");
  // }
  else {

    let loading = this.loadingCtrl.create({});
    loading.present();

    this.memeservice.Login(this.LoginData).then((result) => {
    loading.dismissAll();
    this.data = result;

    let code = this.data.code;
    let message = this.data.message;

    if(code == 2) {
      this.presentAlert("",message);
      this.openRegister();
    }
    else if(code == 1) {

      this.storage.set('MemesUphasLoggedIn', 'true');

      localStorage.setItem("MemesasaaccessToken",this.data.result.appId);
      this.storage.set('MemesasaaccessToken', this.data.result.appId);

    //   if(this.data.result.is_verified == true) {
    //
    //   if(this.data.result.is_fully_registered == 1) {
    //     this.storage.set('Wellnessis_fully_registered', 'true');

    // if(this.intendedPage == "Profile") {
    //   this.app.getRootNav().setRoot(Profile);
    // }
    // else if(this.intendedPage == "PostPage") {
    //   this.app.getRootNav().setRoot(PostPage);
    // }


      //   this.viewCtrl.dismiss();
    //   }
    //   else {
         this.app.getRootNav().setRoot(Profile,{flag:'1'});
    //   }
    //
    // }
    // else {
    //   this.presentAlertResend(this.LoginData.email,"","Your account email is not verified.");
    // }

    }
    else {
      this.presentAlert("",message);
    }

  }, (err) => {
    loading.dismissAll();
    this.presentAlert("",err.error.message);
  });

  }
  }

  // presentAlertResend(email,alerttitle,alertcontent) {
  //   let alert = this.alertCtrl.create({
  //    title: 'Unverified Account',
  //    message: alertcontent,
  //    buttons: [
  //      {
  //        text: 'Resend Mail',
  //        role: 'cancel',
  //        handler: () => {
  //          console.log('Cancel clicked');
  //          this.resendVerificationMail(email);
  //        }
  //      },
  //      {
  //        text: 'Cancel',
  //        handler: () => {
  //          console.log('Buy clicked');
  //        }
  //      },
  //      {
  //        text: 'Ok',
  //        handler: () => {
  //          console.log('Buy clicked');
  //        }
  //      }
  //    ]
  //  });
  //  alert.present();
  // }
  //
  // resendVerificationMail(email) {
  //
  //   if(this.activeLang == "zh" || this.activeLang == "zh-TW") {
  //     this.waittext = '請稍候…';
  //   }
  //   else {
  //     this.waittext = 'Please wait...';
  //   }
  //
  //   let loading = this.loadingCtrl.create({ content: this.waittext });
  //   loading.present();
  //
  //   this.Wellnessservice.resendVerification(email).then((result) => {
  //     loading.dismissAll();
  //     this.data = result;
  //
  //     this.presentAlert('',this.data.message);
  //
  //   }, (err) => {
  //     loading.dismissAll();
  //     this.presentAlert("",err.error.message);
  //   });
  //
  // }

  forgotPassword() {

    let alert = this.alertCtrl.create({
    title: "Recover lost password",
    message:"We will send a password recovery link to your email. Please click on it and follow the prompts.",
    inputs: [
      {
        name: 'email',
        placeholder: "Enter your email here..."
      }
    ],
    buttons: [
      {
        text: "Cancel",
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: "Submit",
        handler: data => {
          if (data.email) {
            // logged in!
            this.recoverPassword(data.email);
          } else {
            // invalid login
            return false;
          }
        }
      }
    ]
  });
  alert.present();
  }

  recoverPassword(email) {
    let loading = this.loadingCtrl.create({ content: 'Please wait...' });
    loading.present();
    this.memeservice.forgotpassword(email)
    .then(result => {
    this.data = result;
    loading.dismissAll();
    //let status_code = this.data.code;
    let message = this.data.message;
    this.presentAlert('',message);
    });
  }

  presentAlert(alerttitle,alertcontent) {
   const alert = this.alertCtrl.create({
     title: alerttitle,
     subTitle: alertcontent,
     buttons: ['Ok']
   });
   alert.present();
  }

  openRegister() {
    this.app.getRootNav().setRoot(SignupPage);
    //this.viewCtrl.dismiss();
    //this.navCtrl.push(SignupPage);
  }

  ionViewDidLoad() {

  }

}
