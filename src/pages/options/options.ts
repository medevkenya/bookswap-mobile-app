import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, LoadingController, AlertController } from 'ionic-angular';
import { MemeProvider } from '../../providers/meme/meme';
import { TermsPage } from '../terms/terms';
import { PrivacyPage } from '../privacy/privacy';
import { ChangepasswordPage } from '../changepassword/changepassword';
import { Profile } from '../profile/profile';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the Options page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-options',
  templateUrl: 'options.html',
})
export class Options {

  public notifyMe;

  data:any;
  loading:any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private app:App,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public memeservice: MemeProvider,
    public storage: Storage
  ) {
    this.notifyMe = this.navParams.get('notifyMe');
  }

  openTerms() {
    this.navCtrl.push(TermsPage);
  }

  changePassword() {
    this.navCtrl.push(ChangepasswordPage);
  }

  openPrivacy() {
    this.navCtrl.push(PrivacyPage);
  }

  goEditProfile() {
    this.navCtrl.push(Profile);
  }

  logout(){
    this.storage.set('MemesUphasLoggedIn', 'false');
    this.storage.remove('MemesUphasLoggedIn');
    this.storage.remove('MemesasaaccessToken');
    this.app.getRootNav().setRoot(TabsPage);
  }

  toggleNotify(notifyMe) {

    let loading = this.loadingCtrl.create({});
    loading.present();

    this.memeservice.toggleNotify(notifyMe).then((result) => {

      loading.dismissAll();
      this.data = result;

      if(this.data.code==1) {
        this.notifyMe = notifyMe;
      }

      if(notifyMe == 1) {
        this.presentAlert("","You will be notified everytime a new meme is posted");
      }
      else {
        this.presentAlert("","You will nolonger receive notifications when new memes are posted");
      }

      //this.presentAlert('',this.data.message);

      //this.navCtrl.pop();

    }, (err) => {
      loading.dismissAll();
      this.presentAlert("","Request not sent. Please try again");
    });

  }

  presentAlert(alerttitle,alertcontent) {
  const alert = this.alertCtrl.create({
   title: alerttitle,
   subTitle: alertcontent,
   buttons: ['Ok']
  });
  alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Options');
  }

}
