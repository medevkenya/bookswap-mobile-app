import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Storage } from '@ionic/storage';
import { ServerUrlProvider } from '../../providers/server-url/server-url';

//import { Observable } from 'rxjs/Observable';HttpResponse

// import {
//   HttpRequest,
//   HttpHandler,
//   HttpEvent,
//   HttpInterceptor,
//   HttpResponse,
//   HttpErrorResponse
// } from '@angular/common/http';
// import { Observable, throwError } from 'rxjs';
// import { map, catchError } from 'rxjs/operators';

/*
  Generated class for the MemeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MemeProvider {

  userToken: string;
  hashedKey:string;
  api_url:string;
  currentVersion:string;

constructor(
  private socialSharing: SocialSharing,
  public http: HttpClient,
  public storage: Storage,
  private serverUrl: ServerUrlProvider
) {
  console.log('Hello MemeProvider Provider');
  this.api_url = serverUrl.url;
  this.hashedKey = serverUrl.hashedKey;
  this.currentVersion = serverUrl.currentVersion;
}

  // this will get called once with constructor execution:
  getToken() {
    this.storage.get('MemesasaaccessToken').then((token) => {
      this.userToken = token;
    });
  };

  getDeviceLanguage() {
    return "en";
  }

  shareApp() {
      let options = {
          message: 'Get daily unlimited memes, that you can download, share, put as whatsapp status and also receive notifications when new memes are posted. Download and install Memesasa App', // not supported on some apps (Facebook, Instagram)
          subject: 'Memesasa App', // fi. for email
          files: ['https://memesasa.alternet.co.ke/images/logo.png'], // an array of filenames either locally or remotely
          url: 'https://play.google.com/store/apps/details?id=memesasa.mobile.app&hl=en_US',
          chooserTitle: 'Share Memesasa App with' // Android only, you can override the default share sheet title
      }
      this.socialSharing.shareWithOptions(options).then((result) => {
          console.log("Share completed? ", result.completed); // On Android apps mostly return false even while it's true
          console.log("Shared to app: ", result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
          this.saveActivity('shareApp','success',0);
      }, (err) => {
          console.log("Sharing failed with message: ", err);
          this.saveActivity('shareApp',err,0);
      });
  }

  saveActivity(activity,message,memeId) {
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId, activity:activity, message:message, memeId:memeId,  hashedKey:this.hashedKey };

    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'saveActivity', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

Login(credentials) {
    let email = credentials.email;
    let password = credentials.email;
    let localinfo = { email:email,  password:password, hashedKey:this.hashedKey };
    //console.log("sentbody23232322 login--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'login', JSON.stringify(localinfo))
      .subscribe(res => { //console.log("Login sucee--"+JSON.stringify(res));
        resolve(res);
      }, (err) => { //console.log("Login err--"+JSON.stringify(err));
        reject(err);
      });
  });
}

forgotpassword(credentials) {
    let email = credentials.email;
    let localinfo = { email:email,  hashedKey:this.hashedKey };
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'forgotPass', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

resendVerification(email) {
    let localinfo = { email:email, hashedKey:this.hashedKey };
    //console.log("sentbody23232322 resendVerification--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'resendVerification', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

socialUser(email,name,gender,birthday)
{
  let localinfo = { email:email, name:name,gender:gender, birthday:birthday, hashedKey:this.hashedKey };
  //console.log("sentbody23232322 socialUser--"+JSON.stringify(localinfo));
  return new Promise((resolve, reject) => {
    this.http.post(this.api_url+'socialUser', JSON.stringify(localinfo))
      .subscribe(res => { //console.log("sentbody23232322 sucee--"+JSON.stringify(res));
        resolve(res);
      }, (err) => { //console.log("sentbody23232322 err--"+JSON.stringify(err));
        reject(err);
      });
  });
}

Register(credentials) {
  let email = credentials.email;
  let fullName = credentials.fullName;
  let nickName = credentials.nickName;
  let password = credentials.email;
  let localinfo = { email:email,  password:password, fullName:fullName, nickName:nickName, hashedKey:this.hashedKey };
  //console.log("sentbody23232322 register--"+JSON.stringify(localinfo));
  return new Promise((resolve, reject) => {
    this.http.post(this.api_url+'register', JSON.stringify(localinfo))
      .subscribe(res => { //console.log("register sucee--"+JSON.stringify(res));
        resolve(res);
      }, (err) => { //console.log("register err--"+JSON.stringify(err));
        reject(err);
      });
  });
}

changePassword(credentials) {
  let appId = localStorage.getItem("MemesasaaccessToken");
  let newPassword = credentials.newPassword;
  let currentPassword = credentials.currentPassword;
  let localinfo = { newPassword:newPassword,  currentPassword:currentPassword, appId:appId, hashedKey:this.hashedKey };
  return new Promise((resolve, reject) => {
    this.http.post(this.api_url+'changePassword', JSON.stringify(localinfo))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

editProfile(credentials) {

  let httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + this.getToken(),
      'language': this.getDeviceLanguage()
    })
  }

  let appId = localStorage.getItem("MemesasaaccessToken");
  let fullName = credentials.fullName;
  let nickName = credentials.nickName;
  let email = credentials.email;
  let gender = credentials.gender;
  let localinfo = { appId:appId, gender:gender, email:email, fullName:fullName, nickName:nickName,  hashedKey:this.hashedKey };
//  console.log("editProfile post--"+JSON.stringify(localinfo));

    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'editProfile', JSON.stringify(localinfo), httpOptions)
        .subscribe(res => { //console.log("editProfile sucee--"+JSON.stringify(res));
          resolve(res);
        }, (err) => { //console.log("editProfile err--"+JSON.stringify(err));
          reject(err);
        });
    });
}

sendMemeText(credentials) {

  let httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + this.getToken(),
      'language': this.getDeviceLanguage()
    })
  }

  let appId = localStorage.getItem("MemesasaaccessToken");
  let description = credentials.description;
  let localinfo = { appId:appId, description:description,  hashedKey:this.hashedKey };
  //console.log("sendMemeText post--"+JSON.stringify(localinfo));

    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'sendMemeText', JSON.stringify(localinfo), httpOptions)
        .subscribe(res => { //console.log("sendMemeText sucee--"+JSON.stringify(res));
          resolve(res);
        }, (err) => { //console.log("sendMemeText err--"+JSON.stringify(err));
          reject(err);
        });
    });

}

getNotifications(pageNumber)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId, pageNumber:pageNumber,  hashedKey:this.hashedKey };
  //  console.log("sentbody23232322 getNotifications--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'getNotifications', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

topList(pageNumber)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId, pageNumber:pageNumber,  hashedKey:this.hashedKey };
    //console.log("sentbody23232322 topList--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'topList', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

viewMeme(id) {
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = {  appId:appId,id:id, hashedKey:this.hashedKey };
    //console.log("sentbody23232322 viewMeme--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'viewMeme', JSON.stringify(localinfo))
        .subscribe(res => { //console.log("viewMeme res--"+JSON.stringify(res));
          resolve(res);
        }, (err) => { //console.log("viewMeme err--"+JSON.stringify(err));
          reject(err);
        });
    });
}

likeItem(id) {
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = {  appId:appId,id:id, hashedKey:this.hashedKey };
    //console.log("sentbody23232322 likeItem--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'likeItem', JSON.stringify(localinfo))
        .subscribe(res => { //console.log("likeItem res--"+JSON.stringify(res));
          resolve(res);
        }, (err) => { //console.log("likeItem err--"+JSON.stringify(err));
          reject(err);
        });
    });
}

popular(pageNumber,order) {
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = {  appId:appId,pageNumber:pageNumber, order:order, hashedKey:this.hashedKey };
    //console.log("sentbody23232322 popular--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'popular', JSON.stringify(localinfo))
        .subscribe(res => { //console.log("sentbody23232322 res--"+JSON.stringify(res));
          resolve(res);
        }, (err) => { //console.log("sentbody23232322 err--"+JSON.stringify(err));
          reject(err);
        });
    });
}

topProfiles()
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId,  hashedKey:this.hashedKey };
    //console.log("sentbody23232322 topProfiles--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'topProfiles', JSON.stringify(localinfo))
      .subscribe(res => { //console.log("topProfiles sucee--"+JSON.stringify(res));
        resolve(res);
      }, (err) => { //console.log("topProfiles err--"+JSON.stringify(err));
        reject(err);
      });
  });
}

todayList()
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId,  hashedKey:this.hashedKey };
    //console.log("sentbody23232322 todayList--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'todayList', JSON.stringify(localinfo))
      .subscribe(res => { //console.log("todayList sucee--"+JSON.stringify(res));
        resolve(res);
      }, (err) => { //console.log("todayList err--"+JSON.stringify(err));
        reject(err);
      });
  });
}

loadProfiles(pageNumber)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId, pageNumber:pageNumber,  hashedKey:this.hashedKey };
    //console.log("sentbody23232322 loadProfiles--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'profiles', JSON.stringify(localinfo))
      .subscribe(res => { //console.log("loadProfiles sucee--"+JSON.stringify(res));
        resolve(res);
      }, (err) => { //console.log("loadProfiles err--"+JSON.stringify(err));
        reject(err);
      });
  });
}

getListByUserId(pageNumber,userId)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId, userId:userId, pageNumber:pageNumber,  hashedKey:this.hashedKey };
    //console.log("sentbody23232322 getListByUserId--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'getListByUserId', JSON.stringify(localinfo))
      .subscribe(res => { //console.log("getListByUserId sucee--"+JSON.stringify(res));
        resolve(res);
      }, (err) => { //console.log("getListByUserId err--"+JSON.stringify(err));
        reject(err);
      });
  });
}

getProfile(pageNumber)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId, pageNumber:pageNumber,  hashedKey:this.hashedKey };
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'getProfile', JSON.stringify(localinfo))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

editPic(imageB64){

  let appId = localStorage.getItem("MemesasaaccessToken");
  let localinfo = { appId:appId, imageB64:imageB64,  hashedKey:this.hashedKey };

  return new Promise((resolve, reject) => {
    this.http.post(this.api_url+'editPic', JSON.stringify(localinfo))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });

}

uploadPhotos(credentials)
{
    //console.log("sentbody23232322 uploadPhotos--"+JSON.stringify(credentials));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'uploadPhotos', JSON.stringify(credentials))
      .subscribe(res => { //console.log("uploadPhotos sucee--"+JSON.stringify(res));
        resolve(res);
      }, (err) => { //console.log("uploadPhotos err--"+JSON.stringify(err));
        reject(err);
      });
  });
}


uploadVideo(videos)
{
  //let appId = localStorage.getItem("MemesasaaccessToken");
    //let localinfo = { videos:videos, appId:appId,  hashedKey:this.hashedKey };
    //console.log("sentbody23232322 uploadVideo--"+JSON.stringify(videos));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'uploadVideo', JSON.stringify(videos))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

removeComment(id)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { id:id, appId:appId,  hashedKey:this.hashedKey };
  //  console.log("sentbody23232322 removeComment--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'removeComment', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

postComment(comment,id)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { id:id, comment:comment, appId:appId,  hashedKey:this.hashedKey };
    //console.log("sentbody23232322 postComment--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'postComment', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

getComments(pageNumber,id)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { id:id, pageNumber:pageNumber, appId:appId,  hashedKey:this.hashedKey };
    //console.log("sentbody23232322 getComments--"+JSON.stringify(localinfo));
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'getComments', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

reaction(credentials)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId,  hashedKey:this.hashedKey };
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'reaction', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

removeItem(id)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { id:id, appId:appId,  hashedKey:this.hashedKey };
    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'removeItem', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

checkVersion()
{
  let appId = localStorage.getItem("MemesasaaccessToken");
  let localinfo = { version:this.currentVersion, appId:appId,  hashedKey:this.hashedKey };

    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'checkVersion', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

toggleNotify(notifyMe)
{
  let appId = localStorage.getItem("MemesasaaccessToken");
  let localinfo = { appId:appId, notifyMe:notifyMe,  hashedKey:this.hashedKey };

    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'toggleNotify', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

SendTokenToServer(fcmToken)
{
    let appId = localStorage.getItem("MemesasaaccessToken");
    let localinfo = { appId:appId, version:this.currentVersion,  fcmToken:fcmToken, hashedKey:this.hashedKey };

    return new Promise((resolve, reject) => {
      this.http.post(this.api_url+'saveFcmToken', JSON.stringify(localinfo))
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}

}
