import { Injectable } from '@angular/core';

/*
  Generated class for the ServerUrlProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServerUrlProvider {

  url;
  hashedKey;
  currentVersion;

  constructor() {
    this.url="https://memesasa.alternet.co.ke/api/";
    this.hashedKey = "123456789";
    this.currentVersion = "0.0.8";
  }

}
