import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { SocialSharing } from '@ionic-native/social-sharing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { FCM } from '@ionic-native/fcm';

import { Home } from '../pages/home/home';
import { PostPopover } from '../pages/home/post-popover';
import { Search } from '../pages/search/search';
import { PostPageModule } from '../pages/post/post.module';
import { Messages } from '../pages/messages/messages';
import { MessageDetail } from '../pages/message-detail/message-detail';
import { NewMessage } from '../pages/new-message/new-message';
import { Notifications } from '../pages/notifications/notifications';
import { Profile } from '../pages/profile/profile';
import { EditProfile } from '../pages/edit-profile/edit-profile';
import { TaggedProfile } from '../pages/tagged-profile/tagged-profile';
import { SavedProfile } from '../pages/saved-profile/saved-profile';
import { Options } from '../pages/options/options';
import { Comments } from '../pages/comments/comments';
import { TabsPage } from '../pages/tabs/tabs';
import { PeoplePageModule } from '../pages/people/people.module';
import { UserPageModule } from '../pages/user/user.module';
import { TermsPageModule } from '../pages/terms/terms.module';
import { PrivacyPageModule } from '../pages/privacy/privacy.module';
import { ChangepasswordPageModule } from '../pages/changepassword/changepassword.module';

import { SigninPageModule } from '../pages/signin/signin.module';
import { SignupPageModule } from '../pages/signup/signup.module';

import { PopularPageModule } from '../pages/popular/popular.module';
import { MemedetailsPageModule } from '../pages/memedetails/memedetails.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
// import { FileChooser } from '@ionic-native/file-chooser';
// import {Base64} from "@ionic-native/base64";
import { MemeProvider } from '../providers/meme/meme';
import { ServerUrlProvider } from '../providers/server-url/server-url';
// import { ImagePicker } from '@ionic-native/image-picker';
import { ImagePicker } from '@ionic-native/image-picker';
import { AndroidPermissions } from '@ionic-native/android-permissions';

var config = {
  backButtonText: '',
  backButtonIcon: 'md-arrow-back',
  iconMode: 'md',
  pageTransition: 'ios',
  mode:'ios'
};

@NgModule({
  declarations: [
    MyApp,
    Home,
    PostPopover,
    Search,
    Messages,
    MessageDetail,
    NewMessage,
    Notifications,
    Profile,
    EditProfile,
    TaggedProfile,
    SavedProfile,
    Options,
    Comments,
    TabsPage
  ],
  imports: [
    PostPageModule,PeoplePageModule,UserPageModule,SigninPageModule,SignupPageModule,
    TermsPageModule,PrivacyPageModule,ChangepasswordPageModule,PopularPageModule,MemedetailsPageModule,
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp,config),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Home,
    PostPopover,
    Search,
    Messages,
    MessageDetail,
    NewMessage,
    Notifications,
    Profile,
    EditProfile,
    TaggedProfile,
    SavedProfile,
    Options,
    Comments,
    TabsPage
  ],
  providers: [
    StatusBar,File,FilePath,FileTransfer,ImagePicker,
    //FileChooser,
    SplashScreen,AndroidPermissions,FCM,
    //Base64,
    Camera,SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MemeProvider,ServerUrlProvider
  ]
})
export class AppModule {}
