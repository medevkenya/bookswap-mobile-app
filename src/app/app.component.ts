import { Component } from '@angular/core';
import { Platform, App, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MemeProvider } from '../providers/meme/meme';
import { MemedetailsPage } from '../pages/memedetails/memedetails';
import { FCM } from '@ionic-native/fcm';

import { TabsPage } from '../pages/tabs/tabs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any = TabsPage;

  data:any;
  loading:any;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private fcm: FCM,
    public memeservice: MemeProvider,
    public alertCtrl: AlertController,
    private app:App
  ) {
    this.initializeApp();
  }

  initializeApp() {

    // // Check if the user has already seen the tutorial
    // this.storage.get('hasSeenTutorialBookswap')
    //   .then((hasSeenTutorialBookswap) => {
    //     if (hasSeenTutorialBookswap) {
    //
    //     //check if is logged in
    //     this.storage.get('BooksUphasLoggedIn')
    //     .then((BooksUphasLoggedIn) => {
    //       if (BooksUphasLoggedIn == "true") {
    //         this.rootPage = HomePage;
    //       }
    //       else {
    //         this.rootPage = HomePage;
    //       }
    //     });
    //     //end check if logged in
    //
    //     }
    //     else {
    //      this.rootPage = TutorialPage;
    //     }
    //
    // });

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      //this.statusBar.overlaysWebView(true);
      this.splashScreen.hide();
      // setTimeout(() => {
      //   this.splashScreen.hide();
      // }, 1000);

  //     //Registration of push in Android and Windows Phone
  //     this.platform.registerBackButtonAction(() => {
  //
  //             //Check if actionSheet is already open
  //             //if (this.actionSheet) {return;}
  //
  // // if (!this.nav.canGoBack()) {
  // //   window.plugins.appMinimize.minimize();
  // // }
  // // else {
  // //   return this.nav.pop();
  // // }
  //
  //               let nav = this.app.getActiveNav();
  //               if (nav.canGoBack()){ //Can we go back?
  //                   nav.pop();
  //               }else{
  //                   let actionSheet = this.actionSheetCtrl.create({
  //                   title: 'Do you want to close the app?',
  //                   buttons: [
  //                       {
  //                       text: 'Yes',
  //                           handler: () => {
  //                               this.platform.exitApp(); //Exit from app
  //                           }
  //                       },{
  //                           text: 'Cancel',
  //                           role: 'cancel',
  //                           handler: () => {
  //                               console.log('Cancel clicked');
  //                           }
  //                       }
  //                   ]
  //                   });
  //                   actionSheet.present();
  //               }
  //           });



      if (this.platform.is('cordova')) {
      //console.log("this is cordova");
      this.initPushNotification();
      }
       else {
         //console.log("this is not cordova");
       }

    });

    //Check for new versions
    this.checkForUpdate();

  }

    initPushNotification() {
  this.fcm.subscribeToTopic('marketing');

  this.fcm.getToken().then(token=>{
    //register token
    this.SendTokenToServer(token);
    //console.log(token);
    //this.presentToast('fcm token 1--'+token);
  })


  let self=this;
  this.fcm.onNotification().subscribe(data=>{
    if(data.wasTapped){
      //Notification was received on device tray and tapped by the user.
      //console.log("Received in background "+JSON.stringify(data));
      //this.presentAlert("Received in background "+JSON.stringify(data));

      self.app.getRootNav().setRoot(MemedetailsPage,{payload:data.details});

    } else {
      //Notification was received in foreground. Maybe the user needs to be notified.
    // Here you should add an alert or something that let's the user know
    // you're about to take them to another page
    //console.log("Received in foreground "+JSON.stringify(data));
    this.presentAlertForeGround(data);

    };
  })

  this.fcm.onTokenRefresh().subscribe(token=>{
    //register token
    this.SendTokenToServer(token);
    //console.log(token);
    //this.presentToast('fcm refresh token--'+token);
  })

  //this.fcm.unsubscribeFromTopic('marketing');
}

// presentAlert(alertcontent) {
// const alert = this.alertCtrl.create({
//  title: "FCM payload",
//  subTitle: alertcontent,
//  buttons: ['Ok']
// });
// alert.present();
// }

presentAlertForeGround(data)
{
  const alert = this.alertCtrl.create({
  title: data.title,
  subTitle: data.body,
  buttons: ['Ok']
  });
  alert.present();
  if(data.target_screen != null) {
    alert.onDidDismiss(() => {
      this.app.getRootNav().setRoot(MemedetailsPage,{payload:data.details});
    });
  }
}

SendTokenToServer(fcmToken) {
  this.memeservice.SendTokenToServer(fcmToken).then((result) => {
  this.data = result;
  }, (err) => {
  //this.presentToast('fcm data--'+this.data);
  localStorage.setItem("MemesasaaccessToken",this.data.result.accessToken);
  });
}

checkForUpdate()
{
  this.memeservice.checkVersion().then((result) => {
  this.data = result;
  //console.log("checkVersion---"+JSON.stringify(this.data));
  //this.notificationsCount = this.data.result.notifications;
  //this.events.publish('UbacelebnotificationsCount:UbacelebversionCheck', this.notificationsCount, this.data.result.code);
  if(this.data.result.code == 1) {

    this.presentAlertCheck(this.data.result.force,this.data.result.title,this.data.result.message,this.data.result.url);
  }

localStorage.setItem("MemesasaaccessToken",this.data.result.accessToken);

  }, (err) => {
  //this.presentToast('fcm data--'+this.data);
  });
}

presentAlertCheck(force,title,message,url) {

  const alert = this.alertCtrl.create({
    title: title,
    subTitle: message,
    buttons: ['Ok']
  });
  alert.present();
  if(force==true) {
    alert.onDidDismiss(() => {  window.open(url, '_system'); });
  }
}

}
